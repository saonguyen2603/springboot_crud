package com.example.TableLanguage.controller;

import com.example.TableLanguage.Entity.TableLanguage;
import com.example.TableLanguage.exeption.TableLanguageNotFoundExeption;
import com.example.TableLanguage.service.TableLanguageService;
import com.example.TableLanguage.dto.TableLanguageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/language")
public class TableLanguageController {

    @Autowired
    private TableLanguageService tableLanguageService;


    @PostMapping
    public ResponseEntity<Object> createTableLanguage(@RequestBody TableLanguageDto tableLanguageDto) {
        if (tableLanguageService.isExistByCode(tableLanguageDto.getCode())) {
            throw new RuntimeException("Code đã tồn tại");
        } else {
            TableLanguage tableLanguage = tableLanguageService.createTableLanguage(tableLanguageDto);
            return new ResponseEntity<>(tableLanguage, HttpStatus.CREATED);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateTableLanguage(@PathVariable("id") int id, @RequestBody TableLanguageDto tableLanguageDto) {
        boolean isTableLanguageExist = tableLanguageService.isTableLanguageExist(id);
        if (isTableLanguageExist) {
            TableLanguage tableLanguage = tableLanguageService.updateTableLanguage(id, tableLanguageDto);
            return ResponseEntity.ok().body(tableLanguage);
        } else {
            throw new TableLanguageNotFoundExeption();
        }
    }

    //
//    @RequestMapping(value = "/TableLanguage/softDelete/{id}", method = RequestMethod.PUT)
//    public ResponseEntity<Object> softDeleteTableLanguage(@PathVariable("id") int id, @RequestBody TableLanguageDto tableLanguageDto) {
//        boolean isTableLanguageExist = tableLanguageService.isTableLanguageExist(id);
//        if (isTableLanguageExist) {
//            TableLanguage tableLanguageRequest = modelMapper.map(tableLanguageDto, TableLanguage.class);
//            TableLanguage tableLanguage =tableLanguageService.softDeleteTableLanguage(id, tableLanguageRequest);
//            TableLanguageDto tableLanguageResponse = modelMapper.map(tableLanguage, TableLanguageDto.class);
//            return ResponseEntity.ok().body(tableLanguageResponse);
//        } else {
//            throw new TableLanguageNotFoundExeption();
//        }
//    }
//
    @PutMapping("/softdelete/{1}")
    public ResponseEntity<Object> softDeleteTableLanguage(@PathVariable("id") int id, @RequestBody TableLanguageDto tableLanguageDto) {
        boolean isTableLanguageExist = tableLanguageService.isTableLanguageExist(id);
        if (isTableLanguageExist) {
            TableLanguage tableLanguage = tableLanguageService.softDeleteTableLanguage(id, tableLanguageDto);
            return ResponseEntity.ok().body(tableLanguage);
        } else {
            throw new TableLanguageNotFoundExeption();
        }
    }
//    @RequestMapping(value = "/TableLanguage", method = RequestMethod.GET)
//    public List<TableLanguageDto> getTableLanguage() {
//        return tableLanguageService.getTableLanguage().stream().map(tableLanguage -> modelMapper.map(tableLanguage, TableLanguageDto.class))
//                .collect(Collectors.toList());
//    }

    @GetMapping
    public List<TableLanguage> getTableLanguage() {
        return tableLanguageService.getTableLanguage();
    }

    @GetMapping("/{id}")
    public TableLanguage getTableLanguage(@PathVariable("id") int id) {
        return tableLanguageService.getTableLanguage(id);
    }

    //
//    @RequestMapping(value = "/TableLanguage/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<Object> deleteTableLanguage(@PathVariable("id") int id) {
//        boolean isTableLanguageExist = tableLanguageService.isTableLanguageExist(id);
//        if (isTableLanguageExist) {
//            tableLanguageService.deleteTableLanguage(id);
//            return new ResponseEntity<>("TableLangue is delete successfully.", HttpStatus.OK);
//        } else {
//            throw new TableLanguageNotFoundExeption();
//        }
//    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteTableLanguage(@PathVariable("id") int id) {
        boolean isTableLanguageExist = tableLanguageService.isTableLanguageExist(id);
        if (isTableLanguageExist) {
            tableLanguageService.deleteTableLanguage(id);
            return new ResponseEntity<>("Table Language is delete successfully.", HttpStatus.OK);
        } else {
            throw new TableLanguageNotFoundExeption();
        }
    }
}
