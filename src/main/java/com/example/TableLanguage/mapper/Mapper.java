package com.example.TableLanguage.mapper;

import com.example.TableLanguage.Entity.TableLanguage;
import com.example.TableLanguage.dto.TableLanguageDto;
import org.mapstruct.MappingTarget;

@org.mapstruct.Mapper
public interface Mapper {
    TableLanguage createTableLanguage1(TableLanguageDto tableLanguageDto);
    TableLanguage updateTableLanguage1(int id, TableLanguageDto tableLanguageDto);

    TableLanguageDto entityToOutputDto(TableLanguage tableLanguage);



}
