package com.example.TableLanguage.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class TableLanguageExceptionController {
    @ExceptionHandler(value = TableLanguageNotFoundExeption.class)
    public ResponseEntity<Object> exception(TableLanguageNotFoundExeption exeption){
        return new ResponseEntity<>("TableLanguage not found", HttpStatus.NOT_FOUND);
    }
}
