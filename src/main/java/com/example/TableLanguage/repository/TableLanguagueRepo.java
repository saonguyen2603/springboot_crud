package com.example.TableLanguage.repository;

import com.example.TableLanguage.Entity.TableLanguage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface TableLanguagueRepo extends JpaRepository<TableLanguage, Integer> {
    boolean existsByCode(String code);
}
