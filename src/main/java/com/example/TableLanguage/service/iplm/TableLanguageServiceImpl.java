package com.example.TableLanguage.service.iplm;

import com.example.TableLanguage.Entity.TableLanguage;
import com.example.TableLanguage.dto.TableLanguageDto;
import com.example.TableLanguage.mapper.Mapper;
import com.example.TableLanguage.repository.TableLanguagueRepo;
import com.example.TableLanguage.service.TableLanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TableLanguageServiceImpl implements TableLanguageService {
    @Autowired
    private TableLanguagueRepo tableLanguagueRepo;

    @Autowired
    private Mapper mapper;

    @Override
    public TableLanguage createTableLanguage(TableLanguageDto tableLanguageDto) {
        tableLanguageDto.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        return tableLanguagueRepo.save(mapper.createTableLanguage1(tableLanguageDto));
    }


    @Override
    public TableLanguage updateTableLanguage(int id, TableLanguageDto tableLanguageDto) {
        tableLanguageDto.setCode(tableLanguageDto.getCode());
        tableLanguageDto.setFlag(tableLanguageDto.getFlag());
        tableLanguageDto.setName(tableLanguageDto.getName());
        tableLanguageDto.setDescription(tableLanguageDto.getDescription());
        tableLanguageDto.setUpdated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        tableLanguageDto.setId(id);
        return tableLanguagueRepo.save(mapper.updateTableLanguage1(id, tableLanguageDto));
    }

    @Override
    public TableLanguage getTableLanguage(int id) {
        Optional<TableLanguage> optional = tableLanguagueRepo.findById(id);
        TableLanguage tableLanguage = optional.get();
        return tableLanguage;
    }

    @Override
    public List<TableLanguage> getTableLanguage() {
        return (List<TableLanguage>) tableLanguagueRepo.findAll();
    }

    @Override
    public void deleteTableLanguage(int id) {
        tableLanguagueRepo.deleteById(id);
    }

    @Override
    public TableLanguage softDeleteTableLanguage(int id, TableLanguageDto tableLanguageDto) {
        tableLanguageDto.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        tableLanguageDto.setId(id);
        return tableLanguagueRepo.save(mapper.updateTableLanguage1(id, tableLanguageDto));
    }

    @Override
    public boolean isTableLanguageExist(int id) {
        return tableLanguagueRepo.existsById(id);
    }

    @Override
    public Boolean isExistByCode(String code) {
       return tableLanguagueRepo.existsByCode(code);
    }
}
