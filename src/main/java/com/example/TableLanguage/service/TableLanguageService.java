package com.example.TableLanguage.service;

import com.example.TableLanguage.Entity.TableLanguage;
import com.example.TableLanguage.dto.TableLanguageDto;

import java.util.List;

public interface TableLanguageService {

    TableLanguage createTableLanguage(TableLanguageDto tableLanguageDto);

    TableLanguage updateTableLanguage(int id, TableLanguageDto tableLanguageDto);

    TableLanguage getTableLanguage(int id);

    List<TableLanguage> getTableLanguage();

    void deleteTableLanguage(int id);

    TableLanguage softDeleteTableLanguage(int id, TableLanguageDto tableLanguageDto);

    boolean isTableLanguageExist(int id);

    Boolean isExistByCode(String code);
}
